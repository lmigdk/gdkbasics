## LMI3D GDK Sample tool

This repository contains a sample tool, that can be compiled under the Gocator Development Kit (GDK) from LMI3D.

The Profile Ellipse tool uses the internal OpenCV library to fit an ellipse to the scanned profile. It also contains an example to use the protection scheme, to only allow the tool to run on specific sensors.

---

## Quick Start

You first need to install the GDK (tool has been tested with GDK 5.2), and the prerequisites for it. Then, clone this repository in the GDK folder, to obtain the gdkbasics folder right next to the Gocator folder.

Finally, execute the generate.py python script and compile the generated solution in Visual Studio.

---

## Protection Scheme

The tool comes with an example to protect the tool from being used on unauthorized sensors. The process can be summarized by:

1. Override the VIsVisible() function of the tool to enable it on selected sensors
2. Upload an encrypted license file on the sensor that contains a list of valid sensor IDs.

To add the protection scheme to your own tool, start by validating that it works for the given tool using the steps below, then simply include the GhlLicensing files in your own solution and generate a license file as explained.

You will need to upload the encrypted license file containing the sensor IDs onto each sensor. A new license file can be created for each new sensor you add to the list. Make sure only the encrypted license files are sent to customers, the original unencrypted files must remain confidential.

---

### Protection Scheme on Sensor

To enable the protection scheme for this tool, follow these steps:

1. Start by 
2. Recompile the solution for sensor.
3. Upgrade the sensor with the generated firmware.
4. Validate that the tool is not present in the list (make sure you are in Profile mode).
5. Create or modify the GdkLicense.xml in the Licensing folder to include your sensor ID.
6. Encrypt the GdkLicense.xml by using this command from the Licensing folder: **GdkLicenseTool.exe encrypt -i GdkLicense.xml -o GdkLicense.user -k Password**
7. Upload the license file to the sensor using this command (replace 12345 with your sensor ID): **GdkLicenseTool.exe upload -s 12345 -i GdkLicense.user -w**
8. Reset the sensor and validate that the tool is now present in the list (make sure you are in Profile mode).


---

### Protection Scheme in Emulator

This can also be done directly in the emulator, for testing. The steps are slightly different:

1. Uncomment the define LICENSING_EXAMPLE in the file GdkProfileEllipseFit.cpp.
2. Recompile the solution for the desired Windows target
3. Start the emulator, choose a Uniform Profile scenario.
4. Validate that the tool is not present in the list.
5. Note the sensor ID in the scenario.
6. Create or modify the GdkLicense.xml in the Licensing folder to include the sensor ID from the scenario.
7. Encrypt the GdkLicense.xml by using this command from the Licensing folder: **GdkLicenseTool.exe encrypt -i GdkLicense.xml -o GdkLicense.user -k Password**
8. Upload the license file to the sensor using this command (replace 12345 with the sensor ID): **GdkLicenseTool.exe upload -s 12345 -i GdkLicense.user -w**
9. Place the file in the scenario's "user" folder. The scenario will be either in %LocalAppData%\LMI\GoEmulate\datasets if you added it yourself, or in GDK\res\GoEmulate\datasets if it is a demo scenario.
10. Restart the emulator and validate that the tool is now present in the list.

[GdkBasics repository](https://bitbucket.org/lmigdk/gdkbasics)