
GdkLicenseTool.h

Copyright (C) 2011 by LMI Technologies Inc.

Licensed under The MIT License.
Redistributions of files must retain the above copyright notice.

Purpose: Encrypt or decrypt a license file for GDK tools IP protection.
         The license file is an xml file containing the list of sensors serial numbers,
         and the tools they are allowed to run. In the tool to protect, a call to a function
         will check the permission in the license file.

         The encrypted file should be placed on the sensor as /user/GdkLicense.user, and must be
         encrypted with the same key used to validate the permission of the tool. Using the
         default key is not recommended, use either a string on the command-line, or provide a
         key file containing the bytes of the key to use.

License file format:
     The license file, before encryption, should look like this:

     <?xml version="1.0" encoding="UTF-8"?>
     <GdkLicense>
       <Version>1.0</Version>
       <!-- There can be as many groups as needed -->
       <Group name="GroupA">
         <Sensors>
           <!-- List of sensors (serial number) to enable the following tools -->
           <Sensor>10001</Sensor>
           <Sensor>20002</Sensor>
           <Sensor>30003</Sensor>
         </Sensors>
         <Tools>
           <!-- List of tools to enable -->
           <!-- The tool name is the internal name, not the displayed name -->
           <Tool>NameOfTool</Tool>
         </Tools>
       </Group>
     </GdkLicense>

To use:

GdkLicenseTool.exe <operation> [-i <inputfile>] [-o <outputfile>] [-f <keyfile>] [-k <key>] [-s <sensorid>] [-w]

  <operation> can be one of the following:
    set:             Set license file (xml) to sensor
    get:             Get license file (xml) from sensor
    upload:          Set license file (encrypted) to sensor
    download:        Get license file (encrypted) from sensor
    encrypt:         Encrypt a file (from xml)
    decrypt:         Decrypt a file (to xml)

  Options:
    -i, --input:     Specifies the name for the input file
    -o, --output:    Specifies the name for the output file
    -f, --keyfile:   Specifies the name of the key file
    -k, --key:       Specifies the key on the command-line, inside double-quotes
    -s, --sensor:    Specifies the sensor id to send to or receive from
    -w, --overwrite: Specifies if allowed to overwrite the output file if it exists


For example:

Encrypt the xml file, with key being the string "Password":
GdkLicenseTool.exe encrypt -i GdkLicense.xml -o GdkLicense.user -k Password

Upload file to sensor, with proper name and location:
GdkLicenseTool.exe upload -s 62735 -i GdkLicense.user -w


