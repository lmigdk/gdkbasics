#include <GdkProfileEllipseFit/GtsAsm.h>
#include <GdkProfileEllipseFit/GdkProfileEllipseFit.h>
#include <GoSensor/Version.h>
#include <Gdk/GdkLib.h>
#include <GoSensorAppLib/GsaDef.h>
#include <GoSensorAppLib/GsaAsm.h>

kBeginAssembly(Gts, GtsAsm, GTS_VERSION, GOCATOR_VERSION)
    kAddDependency(GdkLib)
    kAddType(GdkProfileEllipseFit)
kEndAssembly()

#if !defined(GO_GDK_DYNAMIC)
kStatus kCall GoSensorAppLib_ConstructAssembly(kAssembly* assembly)
{
    return GtsAsm_Construct(assembly);
}
#endif
