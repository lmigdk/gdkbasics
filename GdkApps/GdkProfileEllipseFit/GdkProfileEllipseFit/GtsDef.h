#ifndef GTS_DEF_H
#define GTS_DEF_H

#include <kApi/kApiDef.h>

#if defined(GTS_EMIT)
#    define GtsFx(TYPE)             kInFx(TYPE)
#    define GtsDx(TYPE)             kInDx(TYPE)
#else
#    define GtsFx(TYPE)             kInFx(TYPE)
#    define GtsDx(TYPE)             kInDx(TYPE)
#endif

#endif
