/**
* Copyright (C) 2008-2017 by LMI Technologies Inc.
*/
#ifndef GHL_LICENSING_H
#define GHL_LICENSING_H

//#include <GdkHelperLib/Def.h>
#include <GdkProfileEllipseFit/GtsDef.h>

kBeginHeader()

GtsFx(k32u)    GhlLicensing_GetSensorId();
GtsFx(kArray1) GhlLicensing_GetSensorIds();

GtsFx(kBool)   GhlLicensing_IsToolEnabled(const kChar* toolName, kArray1 key);
GtsFx(kArray1) GhlLicensing_LibraryValidation(kArray1 libKey);

kEndHeader()
#endif
