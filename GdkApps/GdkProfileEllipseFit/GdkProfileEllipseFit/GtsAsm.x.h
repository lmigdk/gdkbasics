#ifndef GTS_ASM_X_H
#define GTS_ASM_X_H

#include <kApi/kApiDef.h>

#define GTS_VERSION     kVersion_Stringify_(1, 0, 0, 2)

kDeclareAssemblyEx(Gts, GtsAsm)

#endif
