#ifndef GTS_ASM_H
#define GTS_ASM_H

#include <GdkProfileEllipseFit/GtsDef.h>

GtsFx(kStatus) GtsAsm_Construct(kAssembly* assembly);

#include <GdkProfileEllipseFit/GtsAsm.x.h>

#endif
