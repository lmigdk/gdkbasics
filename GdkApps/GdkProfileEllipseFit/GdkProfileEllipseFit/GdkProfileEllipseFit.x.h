#ifndef GDK_PROFILE_ELLIPSE_FIT_TOOL_X_H
#define GDK_PROFILE_ELLIPSE_FIT_TOOL_X_H

#include <kApi/Data/kBytes.h>
#include <kApi/Data/kArray2.h>

#include <kVision/S3d/kS3dUtilities.h>

kBeginHeader()

// Macros
#define MATH_SIN_DEG(a)                         sin((a)*kMATH_PI/180)
#define MATH_COS_DEG(a)                         cos((a)*kMATH_PI/180)
#define MATH_DIST_SQ(P0,P1)                    (((P0)->x - (P1)->x)*((P0)->x - (P1)->x) + ((P0)->y - (P1)->y)*((P0)->y - (P1)->y))
#define MATH_DIST(P0,P1)                       (sqrt(MATH_DIST_SQ(P0,P1)))
#define REGION_INIT(region, x, z, w, h)        (region).x = x;(region).z = z;(region).w = w;(region).h = h;

#define GdkRegionXZ64f_IsEqual(a, b)           ((a).x       == (b).x                    && \
                                                (a).z       == (b).z                    && \
                                                (a).width   == (b).width                && \
                                                (a).height  == (b).height)
#define GdkRegionXZ64f_PointIn(r, p)           ((p).x >= (r)->x                         && \
                                                (p).x <= (r)->x + (r)->width            && \
                                                (p).y >= (r)->z                         && \
                                                (p).y <= (r)->z + (r)->height)
#define GdkRegionXZ64f_PointInAnchor(r, a, p)  ((p).x >= (r)->x + (a)->x                && \
                                                (p).x <= (r)->x + (a)->x + (r)->width   && \
                                                (p).y >= (r)->z + (a)->z                && \
                                                (p).y <= (r)->z + (a)->z + (r)->height)

// Enums
enum EllipseFeature {
    ELLIPSE_CENTERX,
    ELLIPSE_CENTERY,
    ELLIPSE_MAJOR,
    ELLIPSE_MINOR,
    ELLIPSE_ANGLE
};

// Constants

#define GDK_PROFILE_ELLIPSE_FIT_TOOL_NAME  "ProfileEllipseFit"
#define GDK_PROFILE_ELLIPSE_FIT_TOOL_LABEL "Profile Ellipse"

#define ELLIPSE_MAX_REGIONS_NUM                 16
#define ELLIPSE_MAX_STR_LEN                     20

#define ELLIPSE_PARAM_NUMBER_OF_REGION_NAME     "numRegions"
#define ELLIPSE_PARAM_NUMBER_OF_REGION_LABEL    "Number of Regions"
#define ELLIPSE_PARAM_NUMBER_OF_REGION_DEFAULT  0

#define ELLIPSE_MEAS_CENTER_X           "CenterX"
#define ELLIPSE_MEAS_CENTER_Y           "CenterY"
#define ELLIPSE_MEAS_CENTER_X_LABEL     "X"
#define ELLIPSE_MEAS_CENTER_Y_LABEL     "Y"
#define ELLIPSE_MEAS_MAJOR              "MajorAxis"
#define ELLIPSE_MEAS_MINOR              "MinorAxis"
#define ELLIPSE_MEAS_STD                "StandardDeviation"
#define ELLIPSE_MEAS_ANGLE              "Angle"

#define ELLIPSE_MEAS_CENTER_X_INDEX     0
#define ELLIPSE_MEAS_CENTER_Y_INDEX     1
#define ELLIPSE_MEAS_MAJOR_INDEX        2
#define ELLIPSE_MEAS_MINOR_INDEX        3
#define ELLIPSE_MEAS_STD_INDEX          4
#define ELLIPSE_MEAS_ANGLE_INDEX        5

#define ELLIPSE_FEATURE_CIRCLE_NAME     "Circle"
#define ELLIPSE_FEATURE_CIRCLE_INDEX    1
#define ELLIPSE_FEATURE_CENTER_NAME     "Center"
#define ELLIPSE_FEATURE_CENTER_INDEX    0

typedef struct
{
    k32s xMin;
    k32s xMax;
    k32s zMin;
    k32s zMax;
} GvProfileEllipseLimit;

typedef struct
{
    k64s sumX;
    k64s sumXSq;
    k64s sumXZ;
    k64s sumZ;
    k64s sumZSq;
    k32s count;
} GvProfileEllipseAccum;

typedef struct
{
    kPoint32s xMin;
    kPoint32s xMax;
    kPoint32s zMin;
    kPoint32s zMax;
} GvProfileEllipseExtrema;

typedef struct
{
    GdkToolClass base;
    GdkDataSource dataSource;

    kSize numRegions;
    GdkRegionXZ64f regions[ELLIPSE_MAX_REGIONS_NUM];

} GdkProfileEllipseFitClass;


kDeclareClass(Gts, GdkProfileEllipseFit, GdkTool)

#define GdkProfileEllipseFit_Cast_(CONTEXT)    kCastClass_(GdkProfileEllipseFit, CONTEXT)

GtsFx(const kChar*) GdkProfileEllipseFit_VName();
GtsFx(kBool) GdkProfileEllipseFit_VIsVisible(const GdkToolEnv* env);
GtsFx(kStatus) GdkProfileEllipseFit_VDescribe(GdkToolInfo info);

GtsFx(kStatus) GdkProfileEllipseFit_VInit(GdkProfileEllipseFit tool, kType type, kAlloc alloc);
GtsFx(kStatus) GdkProfileEllipseFit_VRelease(GdkProfileEllipseFit tool);
GtsFx(kStatus) GdkProfileEllipseFit_VNewToolConfig(const GdkToolEnv* env, GdkToolCfg toolConfig);
GtsFx(kStatus) GdkProfileEllipseFit_VNewMeasurementConfig(const GdkToolEnv* env, GdkToolCfg toolConfig, GdkMeasurementCfg measurementConfig);
GtsFx(kStatus) GdkProfileEllipseFit_VUpdateConfig(const GdkToolEnv* env, GdkToolCfg toolConfig);
GtsFx(kStatus) GdkProfileEllipseFit_VStart(GdkProfileEllipseFit tool);
GtsFx(kStatus) GdkProfileEllipseFit_VStop(GdkProfileEllipseFit tool);
GtsFx(kStatus) GdkProfileEllipseFit_VProcess(GdkProfileEllipseFit tool, GdkToolInput input, GdkToolOutput output);

GtsFx(kStatus) GdkProfileEllipseFit_AllOutputResults(GdkToolCfg config, GdkToolOutput output, k64f value, GdkMeasurementDecision decision);
GtsFx(kStatus) GdkProfileEllipseFit_ExtractPointListFromRegion(GdkProfileEllipseFit tool, GdkInputItem item, const kPoint3d64f* anchor, kArrayList* outPointList, kPoint64f* avgPoint);
GtsFx(kStatus) GdkProfileEllipseFit_ExtractPointListFromRegionTopBottom(GdkProfileEllipseFit tool, GdkInputItem item, GdkInputItem item1, const kPoint3d64f* anchor, kArrayList* outPointList, kPoint64f* avgPoint);


GtsFx(kStatus) GdkProfileEllipseFit_StdDev(kArrayList pointList, kPoint64f* center, k64f radius, k64f* stdev);

GtsFx(kStatus) GdkProfileEllipseFit_OutputValue(GdkToolCfg config, kSize index, k64f value, kBool valid, GdkToolOutput output);
GtsFx(kStatus) GdkProfileEllipseFit_SendFeaturePoint(GdkProfileEllipseFit tool, GdkToolOutput output, kSize index, const kPoint64f* point);

kEndHeader()

#endif
