/**
* Copyright (C) 2008-2017 by LMI Technologies Inc.
*/

#include <GdkProfileEllipseFit/GhlLicensing.h>

#include <kApi/Io/kFile.h>
#include <kApi/Data/kXml.h>
#include <kApi/Data/kArray1.h>
#include <kApi/Data/kString.h>
#include <kFireSync/Utils/kSensorIdentity.h>
#include <kApi/Threads/kThread.h>

#include <GoSensor/version.h>
#include <kFireSync/kFsDef.h>
#include <kFireSync/Client/Network/kDiscovery6Client.h>

#if (GOCATOR_VERSION_MAJOR == 4) && (GOCATOR_VERSION_MINOR < 7)
#include <kFireSync/Crypto/kBlowfishCipher.h>
#include <kFireSync/Crypto/kCipher.h>
#else
// Now in kApi
#include <kApi/Crypto/kBlowfishCipher.h>
#include <kApi/Crypto/kCipher.h>
#endif

#ifdef _WIN32
#include <Windows.h>
#endif

#if (GOCATOR_VERSION_MAJOR < 5)
    #define BEFORE_5_1
#else
    #if (GOCATOR_VERSION_MAJOR == 5 && GOCATOR_VERSION_MINOR == 0)
        #define BEFORE_5_1
    #else
        #define AFTER_5_1
    #endif
#endif


// Blowfish uses block sizes of 64bits, it seems only the 8 first bytes are used anyway.
//#define LIB_VALIDATION_CIPHER_KEY { 0x41, 0xa3, 0x22, 0xb5, 0xe0, 0x23, 0x87, 0x7f, 0xa1 }
#define LIB_VALIDATION_CIPHER_KEY { 0x41, 0xa3, 0x22, 0xb5, 0xe0, 0x23, 0x87, 0x7f, 0x81, 0xa3, 0x22, 0xb5, 0xe0, 0x23, 0x87, 0x7e }
#define LIB_VALIDATION_CIPHER_SIZE 16


kChar* GhlLicensing_GetLine(kChar* dest, k32s capacity, const kChar* start)
{
    // Fills the dest buffer with the line from start to the next EOL character.
    // Returns the pointer after the EOL character.
    const kChar* p1;
    const kChar* p2;
    const kChar* pfinal;
    const kChar* pafter;
    k32s size = capacity;
    p1 = kStrFindFirst(start, "\r");
    p2 = kStrFindFirst(start, "\n");
    pfinal = kMin_(p1, p2) != kNULL ? kMin_(p1, p2) : kMax_(p1, p2);
    size = pfinal != kNULL ? (k32s)(pfinal - start) : (k32s)kStrLength(start);
    size = kMin_(size + 1, capacity);

    dest[0] = '\0';
    kStrCopy(dest, size, start);

    pafter = pfinal != kNULL ? pfinal + 1 : start + kStrLength(start);
    return (kChar*)pafter;
}

kChar* GhlLicensing_SplitLine(kChar* dest, k32s capacity, const kChar* start, const kChar* separator)
{
    // Fills the dest buffer with the first token from start to the next separator character.
    // Returns the pointer after the separator character.
    const kChar* p1;
    const kChar* pafter;
    k32s size = capacity;
    p1 = kStrFindFirst(start, separator);
    size = p1 != kNULL ? (k32s)(p1 - start) : (k32s)kStrLength(start);
    size = kMin_(size + 1, capacity);

    dest[0] = '\0';
    kStrCopy(dest, size, start);

    pafter = p1 != kNULL ? p1 + 1 : start + kStrLength(start);
    return (kChar*)pafter;
}

GtsFx(kStatus) GhlLicensing_WriteLicenseFile(kArray1 key)
{
    kBlowfishCipher bf;
    kArray1 enc;

    kXml license;
    kXmlItem root;
    kXmlItem group;
    kXmlItem item;
    kXmlItem item2;

    kString xmlStr;

    // GdkLicense XSD
    //  <?xml version="1.0" encoding="UTF-8"?>
    //  <xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified" attributeFormDefault="unqualified">
    //    <xs:element name="GdkLicense">
    //      <xs:complexType>
    //        <xs:sequence>
    //          <xs:element name="Version" type="xs:int"/>
    //          <xs:element name="Group" maxOccurs="unbounded">
    //            <xs:complexType>
    //              <xs:sequence>
    //                <xs:element name="Sensors">
    //                  <xs:complexType>
    //                    <xs:sequence>
    //                      <xs:element name="Sensor" maxOccurs="unbounded" type="xs:int"/>
    //                    </xs:sequence>
    //                  </xs:complexType>
    //                </xs:element>
    //                <xs:element name="Tools">
    //                  <xs:complexType>
    //                    <xs:sequence>
    //                      <xs:element name="Tool" type="xs:string"/>
    //                    </xs:sequence>
    //                  </xs:complexType>
    //                </xs:element>
    //              </xs:sequence>
    //              <xs:attribute name="name" type="xs:string"/>
    //            </xs:complexType>
    //          </xs:element>
    //        </xs:sequence>
    //      </xs:complexType>
    //    </xs:element>
    //  </xs:schema>

    // Sample XML
    //  <?xml version="1.0" encoding="UTF-8"?>
    //  <GdkLicense>
    //    <Version>1.0</Version>
    //    <Group name="GroupA">
    //      <Sensors>
    //        <Sensor>10001</Sensor>
    //        <Sensor>36133</Sensor>
    //      </Sensors>
    //      <Tools>
    //        <Tool>SurfaceOperation</Tool>
    //      </Tools>
    //    </Group>
    //    <Group name="GroupB">
    //      <Sensors>
    //        <Sensor>10001</Sensor>
    //        <Sensor>36133</Sensor>
    //      </Sensors>
    //      <Tools />
    //    </Group>
    //  </GdkLicense>

    kXml_Construct(&license, kNULL);
    root = kXml_Root(license);

    kXml_AddItem(license, root, "GdkLicense", &root);

    // Version
    kXml_AddItem(license, root, "Version", &item);
    kXml_SetItemText(license, item, "1.0");

    // Group A
    kXml_AddItem(license, root, "Group", &group);
    kXml_SetAttrText(license, group, "name", "GroupA");

    kXml_AddItem(license, group, "Sensors", &item);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 10001);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 36133);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 10605);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 40954);

    kXml_AddItem(license, group, "Tools", &item);
    kXml_AddItem(license, item, "Tool", &item2);
    kXml_SetItemText(license, item2, "SurfaceOperation");
    kXml_AddItem(license, item, "Tool", &item2);
    kXml_SetItemText(license, item2, "SurfaceGeneralizedHeight");


    // Group B
    kXml_AddItem(license, root, "Group", &group);
    kXml_SetAttrText(license, group, "name", "GroupB");

    kXml_AddItem(license, group, "Sensors", &item);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 10001);
    kXml_AddItem(license, item, "Sensor", &item2);
    kXml_SetItem32u(license, item2, 36133);

    kXml_AddItem(license, group, "Tools", &item);
    kXml_AddItem(license, item, "Tool", &item2);
    kXml_SetItemText(license, item2, "SurfaceOperation");
    //kXml_AddItem(license, item, "Tool", &item2);
    //kXml_SetItemText(license, item2, "SurfaceGeneralizedHeight");


    kString_Construct(&xmlStr, kNULL, kNULL);
    kXml_ToString(license, xmlStr);
    kArray1_Construct(&enc, kTypeOf(kChar), kString_Length(xmlStr) * 2, kNULL);

    kBlowfishCipher_Construct(&bf, (kByte*)kArray1_Data(key), kArray1_Count(key), kCIPHER_PADDING_ANSIX923, kCIPHER_CIPHER_ECB, kNULL);
    kCipher_Encrypt(bf, kString_Chars(xmlStr), kString_Length(xmlStr), enc);

    kFile_Save("/user/GdkLicense.user", (kByte*)kArray1_Data(enc), kArray1_Count(enc));

    // Debugging
    // kXml_Save(license, "/user/GdkLicense.xml");



    return kOK;
}

#define SENSOR_ENUMERATION_COUNT (100)

kStatus GdkLicensing_DiscEnum(kPointer context, kDiscoveryProvider provider, kArrayList info)
{
    k32u* ids = (k32u*)context;
    kDiscoveryInfo *infop = (kDiscoveryInfo*)kArrayList_Data(info);
    kSize i;
    kSize j;

    // Fill the SENSOR_ENUMERATION_COUNT SN we got
    for (i = 0; i < kMin_(SENSOR_ENUMERATION_COUNT, kArrayList_Count(info)); i++)
    {
        for (j = 0; j < SENSOR_ENUMERATION_COUNT; j++)
        {
            if (ids[j] == infop->id)
            {
                // already there
                break;
            }
            if (ids[j] == 0)
            {
                ids[j] = infop->id;
                break;
            }
        }
    }

    return kOK;
}

GtsFx(k32u) GhlLicensing_GetSensorId()
{
    kSensorIdentity identity;
    k32u serial;

    kSensorIdentity_Construct(&identity, kNULL);
    kSensorIdentity_Load(&identity, kSENSOR_IDENTITY_FILENAME, kNULL);
    serial = kSensorIdentity_SerialNumber(identity);
    kDestroyRef(&identity);

#ifdef _WIN32

    // Workaround using discovery to get the first connected sensor, when using the Accelerator
    if (serial == 0)
    {
        k32s i;
        k32u idsa[SENSOR_ENUMERATION_COUNT];
        kDiscovery6Client cl;

#ifdef BEFORE_5_1
        kDiscovery6Client_Construct(&cl, kNULL);
#else
        kDiscovery6Client_Construct(&cl, kNULL, kNULL); //< NOT TESTED
#endif
        kMemSet(&idsa[0], 0, SENSOR_ENUMERATION_COUNT*sizeof(k32u));
        kDiscoveryProvider_SetEnumHandler(cl, (kDiscoveryEnumFx)GdkLicensing_DiscEnum, &idsa[0]);
        kDiscoveryProvider_StartEnum(cl);
        kDiscoveryProvider_WaitFirst(cl);
        kDiscoveryProvider_StopEnum(cl);

        for (i = 0; i < SENSOR_ENUMERATION_COUNT; i++)
        {
            k32u id = idsa[i];
            if (id != 0)
            {
                serial = id;
                break;
            }
        }

        kDestroyRef(&cl);
    }

    //HMODULE kapimod = GetModuleHandle(TEXT("kApi.dll"));
    //HMODULE sdkmod = GetModuleHandle(TEXT("GoSdk.dll"));
    //FARPROC p = GetProcAddress(sdkmod, "GoSensor_Connect");

#endif

    return serial;
}

GtsFx(kArray1) GhlLicensing_GetSensorIds()
{
    kSensorIdentity identity;
    k32u serial;
    kArrayList serialArray = kNULL;
    kArray1 serialArray1 = kNULL;
    k32u* serialPtr;

    k32s i;
    k32u idsa[SENSOR_ENUMERATION_COUNT];
    kMemSet(&idsa[0], 0, SENSOR_ENUMERATION_COUNT*sizeof(k32u));

    kSensorIdentity_Construct(&identity, kNULL);
    kSensorIdentity_Load(&identity, kSENSOR_IDENTITY_FILENAME, kNULL);
    serial = kSensorIdentity_SerialNumber(identity);
    kDestroyRef(&identity);

    kArrayList_Construct(&serialArray, kTypeOf(k32u), SENSOR_ENUMERATION_COUNT, kNULL);
    if (serial != 0)
        kArrayList_Add(serialArray, &serial);

#ifdef _WIN32

    // Workaround using discovery to get the first connected sensor, when using the Accelerator
    if (serial == 0)
    {
        kDiscovery6Client cl;
#ifdef BEFORE_5_1
        kDiscovery6Client_Construct(&cl, kNULL);
#else
        kDiscovery6Client_Construct(&cl, kNULL, kNULL); //< NOT TESTED
#endif
        kDiscoveryProvider_SetEnumHandler(cl, (kDiscoveryEnumFx)GdkLicensing_DiscEnum, &idsa[0]);
        kDiscoveryProvider_StartEnum(cl);
        kThread_Sleep(4*1000*1000); // 1 more second for a max of SENSOR_ENUMERATION_COUNT sensors.
        kDiscoveryProvider_WaitFirst(cl);
        kDiscoveryProvider_StopEnum(cl);

        for (i = 0; i < SENSOR_ENUMERATION_COUNT; i++)
        {
            k32u id = idsa[i];
            if (id != 0)
            {
                kArrayList_Add(serialArray, &id);
            }
        }

        kDestroyRef(&cl);
    }

    //HMODULE kapimod = GetModuleHandle(TEXT("kApi.dll"));
    //HMODULE sdkmod = GetModuleHandle(TEXT("GoSdk.dll"));
    //FARPROC p = GetProcAddress(sdkmod, "GoSensor_Connect");

#endif

    // Convert kArrayList to kArray1
    kArray1_Construct(&serialArray1, kTypeOf(k32u), kArrayList_Count(serialArray), kNULL);
    serialPtr = (k32u*)kArrayList_Data(serialArray);
    for (i = 0; i < (k32s)kArrayList_Count(serialArray); i++)
        kArray1_SetItem(serialArray1, i, &(serialPtr[i]));
    kDestroyRef(&serialArray);

    return serialArray1;
}

kArray1 GhlLicensing_LoadLicenseFile(kArray1 key)
{
    kBlowfishCipher bf;
    kArray1 dec;

    kFile file;
    k32s size;
    kChar* buffer;

    const kChar* filename1 = "/user/GdkLicense.user";
    const kChar* filename2 = "GdkLicense.user";
    const kChar* filename = filename1;

    kFile_Construct(&file, filename, kFILE_MODE_READ, kNULL);

    // File not present, try the fallback, otherwise return invalid
    if (file == kNULL)
    {
        kFile_Construct(&file, filename2, kFILE_MODE_READ, kNULL);
        if (file == kNULL)
            return kNULL;

        filename = filename2;
    }

    // File empty, return invalid
    size = (k32s)kFile_Length(file);
    if (size == 0)
    {
        kDestroyRef(&file);
        return kNULL;
    }

    // Load and store in buffer the plain text of the file.
    kDestroyRef(&file);
    kMemAlloc(size + 1, &buffer);

    kFile_LoadTo(filename, buffer, size);
    buffer[size] = '\0';

    // Decrypt the license file
    kArray1_Construct(&dec, kTypeOf(kChar), size, kNULL);

    kBlowfishCipher_Construct(&bf, (kByte*)kArray1_Data(key), kArray1_Count(key), kCIPHER_PADDING_ANSIX923, kCIPHER_CIPHER_ECB, kNULL);
    kCipher_Decrypt(bf, buffer, size, dec);

    kMemFree(buffer);
    return dec;
}

GtsFx(kBool) GhlLicensing_IsToolEnabled(const kChar* toolName, kArray1 key)
{
    // Check file
    kXml license;
    kString xmlStr;

    kXmlItem root;
    kXmlItem item;
    kXmlItem sensors;
    kXmlItem sensor;
    kXmlItem tools;
    kXmlItem tool;

    kChar version[64];
    kChar toolXml[1025];

    k32u serialXml = 0;

    kArray1 serials = kNULL;
    k32u* serialsPtr = kNULL;
    k32s serialsCount = 0;
    k32s i = 0;

    kArray1 buffer = GhlLicensing_LoadLicenseFile(key);
    if (buffer == kNULL)
        return kFALSE;

    serials = GhlLicensing_GetSensorIds();
    serialsPtr = (k32u*)kArray1_Data(serials);
    serialsCount = (k32s)kArray1_Count(serials);

    // Build the xml from the decrypted file
    kXml_Construct(&license, kNULL);
    kString_Construct(&xmlStr, (kChar*)kArray1_Data(buffer), kNULL);

    kXml_FromString(license, xmlStr);

    root = kXml_Root(license);
    kXml_FindChild(license, root, "Version", &item);
    kXml_ItemText(license, item, version, 64);

    if (kStrCompare(version, "1.0") == 0)
    {
        // Iterate the groups to find the pair serial-tool
        while ((item = kXml_NextSibling(license, item)) != kNULL)
        {
            kXml_FindChild(license, item, "Sensors", &sensors);
            kXml_FindChild(license, sensors, "Sensor", &sensor);
            while (sensor != kNULL)
            {
                kXml_Item32u(license, sensor, &serialXml);
                for (i = 0; i < serialsCount; ++i)
                {
                    if (serialsPtr[i] == serialXml)
                    {
                        kXml_FindChild(license, item, "Tools", &tools);
                        kXml_FindChild(license, tools, "Tool", &tool);
                        while (tool != kNULL)
                        {
                            kXml_ItemText(license, tool, &toolXml[0], 1024);
                            if (kStrCompare(toolXml, toolName) == 0)
                            {
                                // ...And the tool matches, return ok.
                                kDestroyRef(&buffer);
                                kDestroyRef(&xmlStr);
                                kDestroyRef(&license);
                                kDestroyRef(&serials);
                                return kTRUE;
                            }
                            tool = kXml_NextSibling(license, tool);
                        }
                    }
                }
                sensor = kXml_NextSibling(license, sensor);
            }
        }
    }

    kDestroyRef(&buffer);
    kDestroyRef(&xmlStr);
    kDestroyRef(&license);
    kDestroyRef(&serials);
    return kFALSE;
}

GtsFx(kArray1) GhlLicensing_LibraryValidation(kArray1 encryptedData)
{
    kBlowfishCipher bf;
    kArray1 dec;

    kByte key[LIB_VALIDATION_CIPHER_SIZE] = LIB_VALIDATION_CIPHER_KEY;

    // Decrypt encryptedData and send the result, as a validation that the lib knows the key.
    kArray1_Construct(&dec, kTypeOf(kChar), kArray1_Count(encryptedData), kNULL);

    kBlowfishCipher_Construct(&bf, key, LIB_VALIDATION_CIPHER_SIZE, kCIPHER_PADDING_ANSIX923, kCIPHER_CIPHER_ECB, kNULL);
    kCipher_Decrypt(bf, kArray1_Data(encryptedData), kArray1_Count(encryptedData), dec);
    kDestroyRef(&bf);

    return dec;
}
