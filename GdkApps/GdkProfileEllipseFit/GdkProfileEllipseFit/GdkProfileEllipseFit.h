#ifndef GDK_PROFILE_ELLIPSE_FIT_TOOL_H
#define GDK_PROFILE_ELLIPSE_FIT_TOOL_H

#include <GdkProfileEllipseFit/GtsDef.h>
#include <Gdk/Tools/GdkTool.h>

kBeginHeader()

typedef GdkTool GdkProfileEllipseFit;

kEndHeader()

#include <GdkProfileEllipseFit/GdkProfileEllipseFit.x.h>

#endif 
