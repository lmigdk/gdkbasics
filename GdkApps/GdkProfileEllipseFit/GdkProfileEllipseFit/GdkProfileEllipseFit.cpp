/////////////////////////////////////////////////////////////////////////////
// GdkProfileEllipseFit
// 2016-04-06 (C) LMI Technologies
//
// Tool to compute the spDEBUG parameter of in a region.
/////////////////////////////////////////////////////////////////////////////

#include <GdkProfileEllipseFit/GdkProfileEllipseFit.h>

#include <kApi/Data/kArray1.h>
#include <kApi/Data/kMath.h>
#include <kApi/Threads/kTimer.h>
#include <kVision/Common/kNullMath.h>
#include <math.h>

#include <opencv2/imgproc.hpp>

// Uncomment to protect the tool with GdkLicense.user file.
//#define LICENSING_EXAMPLE

#ifdef LICENSING_EXAMPLE
#include <GdkProfileEllipseFit/GhlLicensing.h>
#endif

kBeginClass(Gts, GdkProfileEllipseFit, GdkTool)
kAddVMethod(GdkProfileEllipseFit, kObject, VRelease)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VInit)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VName)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VDescribe)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VNewToolConfig)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VNewMeasurementConfig)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VUpdateConfig)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VStart)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VStop)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VProcess)
kAddVMethod(GdkProfileEllipseFit, GdkTool, VIsVisible)
kEndClass()

// Cpp Function Headers

GtsFx(kStatus) GdkProfileEllipseFit_cvFitElipse(kArrayList pointList, k32s angleOffset, cv::RotatedRect &outRect);
GtsFx(kStatus) GdkProfileEllipseFit_OutputValueAndEllipse(GdkToolCfg config, kSize index, cv::RotatedRect ellipseRect, EllipseFeature feature, GdkToolOutput output, kAlloc alloc);

/////////////////////////////////////////////////////////////////////////////
// GtTool functions
/////////////////////////////////////////////////////////////////////////////

GtsFx(const kChar*) GdkProfileEllipseFit_VName()
{
    return GDK_PROFILE_ELLIPSE_FIT_TOOL_NAME;
}

GtsFx(kBool) GdkProfileEllipseFit_VIsVisible(const GdkToolEnv* env)
{
#ifdef LICENSING_EXAMPLE
    const kByte KEY_DATA[9] = "Password";
    const kSize KEY_SIZE = 8; //< Final NULL character is excluded

    kArray1 key = kNULL;
    kArray1_Construct(&key, kTypeOf(kByte), KEY_SIZE, kNULL);
    kArray1_Attach(key, (void*)KEY_DATA, kTypeOf(kByte), KEY_SIZE);
    if (!GhlLicensing_IsToolEnabled(GDK_PROFILE_ELLIPSE_FIT_TOOL_NAME, key))
    {
        return kFALSE;
    }
#endif
    return kTRUE;
}

GtsFx(kStatus) GdkProfileEllipseFit_VDescribe(GdkToolInfo toolInfo)
{
    GdkParamInfo paramInfo = kNULL;
    GdkMeasurementInfo mmtInfo = kNULL;
    GdkFeatureInfo featureInfo = kNULL;
    GdkRegion3d64f defRegion = { 0,0,1,1 };
    kSize i = 0;
    kSize defInt = ELLIPSE_PARAM_NUMBER_OF_REGION_DEFAULT;

    kCheck(GdkToolInfo_SetLabel(toolInfo, GDK_PROFILE_ELLIPSE_FIT_TOOL_LABEL));
    kCheck(GdkToolInfo_SetTypeName(toolInfo, GDK_PROFILE_ELLIPSE_FIT_TOOL_NAME));

    kCheck(GdkToolInfo_SetSourceType(toolInfo, GDK_DATA_TYPE_UNIFORM_PROFILE));
    kCheck(GdkToolInfo_AddSourceOption(toolInfo, GDK_DATA_SOURCE_TOP));
    kCheck(GdkToolInfo_AddSourceOption(toolInfo, GDK_DATA_SOURCE_BOTTOM));
    kCheck(GdkToolInfo_AddSourceOption(toolInfo, GDK_DATA_SOURCE_TOP_BOTTOM));

    kCheck(GdkToolInfo_EnableAnchoring(toolInfo, GDK_AXIS_X, kTRUE));
    kCheck(GdkToolInfo_EnableAnchoring(toolInfo, GDK_AXIS_Z, kTRUE));

    // Add the input regions
    kCheck(GdkToolInfo_AddParam(toolInfo, GDK_PARAM_TYPE_INT, ELLIPSE_PARAM_NUMBER_OF_REGION_NAME, ELLIPSE_PARAM_NUMBER_OF_REGION_LABEL, &defInt, &paramInfo));
    kCheck(GdkParamInfo_AddOptionInt(paramInfo, 0, "0 (Use all Profile)"));
    for (i = 1; i <= ELLIPSE_MAX_REGIONS_NUM; i++)
    {
        kChar regionName[ELLIPSE_MAX_STR_LEN] = "", strNum[ELLIPSE_MAX_STR_LEN] = "";

        kCheck(kStrPrintf(regionName, ELLIPSE_MAX_STR_LEN, "Region%d", i));
        kCheck(kStrPrintf(strNum, ELLIPSE_MAX_STR_LEN, "%d", i));

        kCheck(GdkParamInfo_AddOptionInt(paramInfo, (k32s)i, strNum));
        kCheck(GdkToolInfo_AddParam(toolInfo, GDK_PARAM_TYPE_PROFILE_REGION, regionName, regionName, &defRegion, kNULL));
    }

    // Output Measurements
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_CENTER_X, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetLabel(mmtInfo, ELLIPSE_MEAS_CENTER_X_LABEL));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_X));
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_CENTER_Y, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetLabel(mmtInfo, ELLIPSE_MEAS_CENTER_Y_LABEL));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_Y));
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_MAJOR, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_VALUE));
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_MINOR, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_VALUE));
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_STD, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_VALUE));
    kCheck(GdkToolInfo_AddMeasurement(toolInfo, ELLIPSE_MEAS_ANGLE, &mmtInfo));
    kCheck(GdkMeasurementInfo_SetValueType(mmtInfo, GDK_MEASUREMENT_VALUE_TYPE_Y_ANGLE));

    // Output Features
    kCheck(GdkToolInfo_AddFeature(toolInfo, ELLIPSE_FEATURE_CENTER_NAME, GDK_FEATURE_TYPE_POINT, &featureInfo));
    //kCheck(GdkToolInfo_AddFeature(toolInfo, ELLIPSE_FEATURE_CIRCLE_NAME, GDK_FEATURE_TYPE_CIRCLE, &featureInfo));

    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VInit(GdkProfileEllipseFit tool, kType type, kAlloc alloc)
{
    kCheck(GdkTool_VInit(tool, type, alloc));
    kInitFields(GdkProfileEllipseFit, tool);

    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VRelease(GdkProfileEllipseFit tool)
{
    return GdkTool_VRelease(tool);
}

GtsFx(kStatus) GdkProfileEllipseFit_VNewToolConfig(const GdkToolEnv* env, GdkToolCfg toolConfig)
{
    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VNewMeasurementConfig(const GdkToolEnv* env, GdkToolCfg toolConfig, GdkMeasurementCfg measurementConfig)
{
    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VUpdateConfig(const GdkToolEnv* env, GdkToolCfg toolConfig)
{
    kSize i = 0;
    GdkParams params = GdkToolCfg_Parameters(toolConfig);
    GdkParam paramUseRefRegion = GdkParams_Find(params, ELLIPSE_PARAM_NUMBER_OF_REGION_NAME);

    kSize numRegions = GdkParam_AsBool(paramUseRefRegion);

    for (i = 1; i <= ELLIPSE_MAX_REGIONS_NUM; i++)
    {
        kChar regionName[ELLIPSE_MAX_STR_LEN] = "";
        kCheck(kStrPrintf(regionName, ELLIPSE_MAX_STR_LEN, "Region%d", i));

        GdkParam paramRefRegion = GdkParams_Find(params, regionName);
        kCheck(GdkParam_SetUsed(paramRefRegion, i <= numRegions));
    }

    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VStart(GdkProfileEllipseFit tool)
{
    GdkProfileEllipseFitClass* obj = GdkProfileEllipseFit_Cast_(tool);
    GdkToolCfg config = GdkTool_Config(tool);
    GdkParams params = GdkToolCfg_Parameters(config);
    const GdkRegionXZ64f zeroRegion = { 0,0,0,0 };

    GdkParam regionParam;
    kSize i = 0;

    obj->dataSource = GdkToolCfg_Source(config);

    GdkParam paramNumRegion = GdkParams_Find(params, ELLIPSE_PARAM_NUMBER_OF_REGION_NAME);
    obj->numRegions = GdkParam_AsInt(paramNumRegion);

    for (i = 0; i < ELLIPSE_MAX_REGIONS_NUM; i++)
    {
        kChar regionName[ELLIPSE_MAX_STR_LEN] = "";
        kCheck(kStrPrintf(regionName, ELLIPSE_MAX_STR_LEN, "Region%d", i + 1));
        regionParam = GdkParams_Find(params, regionName);

        if (i < obj->numRegions)
        {
            GdkRegionXZ64f region = *(GdkRegionXZ64f*)GdkParam_AsProfileRegion(regionParam);
            if (GdkRegionXZ64f_IsEqual(region, zeroRegion))
            {
                GdkParam_SetProfileRegion(regionParam, &obj->regions[i]);
            }
            else
            {
                obj->regions[i] = region;
            }
        }
        else
        {
            GdkParam_SetProfileRegion(regionParam, &zeroRegion);
        }
    }

    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VStop(GdkProfileEllipseFit tool)
{
    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_VProcess(GdkProfileEllipseFit tool, GdkToolInput input, GdkToolOutput output)
{
    kStatus status = kOK;
    GdkProfileEllipseFitClass* obj = GdkProfileEllipseFit_Cast_(tool);
    GdkToolCfg config = GdkTool_Config(tool);
    GdkInputItem item = GdkToolInput_Find(input, obj->dataSource);
    GdkInputItem item1 = kNULL;
    if (obj->dataSource == 4)
    {
        item = GdkToolInput_Find(input, 0);
        item1 = GdkToolInput_Find(input, 1);
    }
    const kPoint3d64f* anchor = GdkToolInput_AnchorPosition(input);
    kArrayList pointListRef = kNULL;
    kArray1 pointListEllipse = kNULL;
    kAlloc alloc = kObject_Alloc(tool);

    kPoint64f center = {k64F_NULL};
    k64f stdev = k64F_NULL;

    cv::RotatedRect ellipseRect;

    if (!item) return kERROR_PARAMETER;

    kTry
    {
        // Check if anchor is valid
        if (anchor->x == k64F_NULL || anchor->y == k64F_NULL)
        {
            kThrow(-GDK_MEASUREMENT_ERROR_ANCHOR);
        }

        // Extract required points from regions
        if (obj->dataSource == 4)
        {
            kTest(GdkProfileEllipseFit_ExtractPointListFromRegionTopBottom(tool, item, item1, anchor, &pointListRef, kNULL));
        }
        else
        {
            kTest(GdkProfileEllipseFit_ExtractPointListFromRegion(tool, item, anchor, &pointListRef, kNULL));
        }
        // Fit a spDEBUG and get the standard deviation
        kTest(GdkProfileEllipseFit_cvFitElipse(pointListRef, 0, ellipseRect));

        //kTest(GdkProfileEllipseFit_StdDev(pointListRef, &center, radius, &stdev));

        // Output Measurements
        kTest(GdkProfileEllipseFit_OutputValueAndEllipse(config, ELLIPSE_MEAS_CENTER_X_INDEX, ellipseRect, ELLIPSE_CENTERX, output, alloc));
        kTest(GdkProfileEllipseFit_OutputValueAndEllipse(config, ELLIPSE_MEAS_CENTER_Y_INDEX, ellipseRect, ELLIPSE_CENTERY, output, alloc));
        kTest(GdkProfileEllipseFit_OutputValueAndEllipse(config, ELLIPSE_MEAS_MAJOR_INDEX, ellipseRect, ELLIPSE_MAJOR, output, alloc));
        kTest(GdkProfileEllipseFit_OutputValueAndEllipse(config, ELLIPSE_MEAS_MINOR_INDEX, ellipseRect, ELLIPSE_MINOR, output, alloc));
        kTest(GdkProfileEllipseFit_OutputValueAndEllipse(config, ELLIPSE_MEAS_ANGLE_INDEX, ellipseRect, ELLIPSE_ANGLE, output, alloc));
        kTest(GdkProfileEllipseFit_OutputValue(config, ELLIPSE_MEAS_STD_INDEX, stdev, stdev != k64F_NULL, output));

        // Output Features
        kTest(GdkProfileEllipseFit_SendFeaturePoint(tool, output, ELLIPSE_FEATURE_CENTER_INDEX, &center));
        //kTest(GdkProfileEllipseFit_SendFeatureCircle(tool, output, ELLIPSE_FEATURE_CIRCLE_INDEX, &center, radius));
    }
    kCatchEx(&status)
    {
        if (status == -GDK_MEASUREMENT_ERROR_ANCHOR)
        {
            GdkProfileEllipseFit_AllOutputResults(config, output, k64F_NULL, GDK_MEASUREMENT_ERROR_ANCHOR);
        }
        else
        {
            GdkProfileEllipseFit_AllOutputResults(config, output, k64F_NULL, GDK_MEASUREMENT_ERROR_VALUE);
        }
        kEndCatchEx(kOK);
    }
    kFinallyEx
    {
        kDestroyRef(&pointListRef);
        kDestroyRef(&pointListEllipse);
        kEndFinallyEx();
    }

    return kOK;
}

/////////////////////////////////////////////////////////////////////////////
// Private tool functions
/////////////////////////////////////////////////////////////////////////////

// Sets all the outputs to the same value and decision (used for errors)
GtsFx(kStatus) GdkProfileEllipseFit_AllOutputResults(GdkToolCfg config, GdkToolOutput output, k64f value, GdkMeasurementDecision decision)
{
    for (kSize i = 0; i < GdkToolCfg_MeasurementCount(config); i++)
    {
        if (GdkMeasurementCfg_Enabled(GdkToolCfg_MeasurementAt(config, i)))
        {
            kCheck(GdkToolOutput_SetResult(output, i, value, decision));
        }
    }

    return kOK;
}

// Create a list of all the points in all the regions combined
GtsFx(kStatus) GdkProfileEllipseFit_ExtractPointListFromRegion(GdkProfileEllipseFit tool, GdkInputItem item, const kPoint3d64f* anchor, kArrayList* outPointList, kPoint64f* outAvgPoint)
{
    GdkProfileEllipseFitClass* obj = GdkProfileEllipseFit_Cast_(tool);
    GdkDataInfo itemInfo = GdkInputItem_Info(item);
    kSize regionIdx = 0;
    kSize columns = 0;
    kSize i = 0;
    k16s k = 0;
    const kPoint3d64f* offset = kNULL, *scale = kNULL;
    kPoint64f point = { k64F_NULL, k64F_NULL };
    kPoint64f avgPoint = {0, 0};

    columns = GdkProfileInput_Count(item);
    offset = GdkInputItem_Offset(item);
    scale = GdkDataInfo_Scale(itemInfo);

    kCheck(kArrayList_Construct(outPointList, kTypeOf(kPoint64f), 0, kObject_Alloc(tool)));

    const k16s* pixel = GdkProfileInput_Ranges(item);
    for (i = 0; i < columns; i++)
    {
        k = pixel[i];
        if (k != k16S_NULL)
        {
            point.x = i * scale->x + offset->x;
            point.y = k * scale->z + offset->z;

            kBool pointInRegions = kFALSE;
            for (regionIdx = 0; regionIdx < obj->numRegions; regionIdx++)
            {
                if (GdkRegionXZ64f_PointInAnchor(&obj->regions[regionIdx], anchor, point))
                {
                    pointInRegions = kTRUE;
                    break;
                }
            }

            if (obj->numRegions == 0 || pointInRegions)
            {
                kCheck(kArrayList_Add(*outPointList, &point));
                avgPoint.x += point.x;
                avgPoint.y += point.y;
            }
        }
    }

    if (kArrayList_Count(*outPointList) > 0) {
        avgPoint.x /= kArrayList_Count(*outPointList);
        avgPoint.y /= kArrayList_Count(*outPointList);
    }

    if (outAvgPoint != kNULL)
    {
        outAvgPoint->x = avgPoint.x;
        outAvgPoint->y = avgPoint.y;
    }
    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_ExtractPointListFromRegionTopBottom(GdkProfileEllipseFit tool, GdkInputItem item, GdkInputItem item1, const kPoint3d64f* anchor, kArrayList* outPointList, kPoint64f* outAvgPoint)
{
    GdkProfileEllipseFitClass* obj = GdkProfileEllipseFit_Cast_(tool);
    GdkDataInfo itemInfo = GdkInputItem_Info(item);
    kSize regionIdx = 0;
    kSize columns = 0;
    kSize i = 0;
    k16s k = 0;
    const kPoint3d64f* offset = kNULL, *scale = kNULL;
    kPoint64f point = { k64F_NULL, k64F_NULL };
    kPoint64f avgPoint = { 0, 0 };

    columns = GdkProfileInput_Count(item);
    offset = GdkInputItem_Offset(item);
    scale = GdkDataInfo_Scale(itemInfo);

    kCheck(kArrayList_Construct(outPointList, kTypeOf(kPoint64f), 0, kObject_Alloc(tool)));

    const k16s* pixel = GdkProfileInput_Ranges(item);
    const k16s* pixel1 = GdkProfileInput_Ranges(item1);
    for (i = 0; i < columns; i++)
    {
        k = pixel[i];
        if (k != k16S_NULL)
        {
            point.x = i * scale->x + offset->x;
            point.y = k * scale->z + offset->z;

            kBool pointInRegions = kFALSE;
            for (regionIdx = 0; regionIdx < obj->numRegions; regionIdx++)
            {
                if (GdkRegionXZ64f_PointInAnchor(&obj->regions[regionIdx], anchor, point))
                {
                    pointInRegions = kTRUE;
                    break;
                }
            }

            if (obj->numRegions == 0 || pointInRegions)
            {
                kCheck(kArrayList_Add(*outPointList, &point));
                avgPoint.x += point.x;
                avgPoint.y += point.y;
            }
        }
    }

    for (i = 0; i < columns; i++)
    {
        k = pixel1[i];
        if (k != k16S_NULL)
        {
            point.x = i * scale->x + offset->x;
            point.y = k * scale->z + offset->z;

            kBool pointInRegions = kFALSE;
            for (regionIdx = 0; regionIdx < obj->numRegions; regionIdx++)
            {
                if (GdkRegionXZ64f_PointInAnchor(&obj->regions[regionIdx], anchor, point))
                {
                    pointInRegions = kTRUE;
                    break;
                }
            }

            if (obj->numRegions == 0 || pointInRegions)
            {
                kCheck(kArrayList_Add(*outPointList, &point));
                avgPoint.x += point.x;
                avgPoint.y += point.y;
            }
        }
    }

    if (kArrayList_Count(*outPointList) > 0) {
        avgPoint.x /= kArrayList_Count(*outPointList);
        avgPoint.y /= kArrayList_Count(*outPointList);
    }

    if (outAvgPoint != kNULL)
    {
        outAvgPoint->x = avgPoint.x;
        outAvgPoint->y = avgPoint.y;
    }
    return kOK;
}


GtsFx(kStatus) GdkProfileEllipseFit_cvFitElipse(kArrayList pointList, k32s angleOffset, cv::RotatedRect &outRect)
{
    kCheck(kArrayList_Count(pointList) > 3);

    std::vector<cv::Point2f> points = std::vector<cv::Point2f>();

    for (int i = 0; i < (int)kArrayList_Count(pointList); i++)
    {
        kPoint64f* pt = (kPoint64f*)kArrayList_At(pointList, i);
        points.push_back(cv::Point2f((k32f)pt->x, (k32f)pt->y));
    }

    cv::Mat m = cv::Mat(points);
    outRect = cv::fitEllipse(m);
    outRect.angle += angleOffset;

    return kOK;
}

// Find the standart deviation of the fit
GtsFx(kStatus) GdkProfileEllipseFit_StdDev(kArrayList pointList, kPoint64f* center, k64f radius, k64f* stdev)
{
    kPoint3d64f* pointPtr = kNULL;
    kStatus exception = kOK;
    k32s ix;

    k64s pointCount = 0;
    k64f Xsum = 0;

    double var = 0;

    kTry
    {
        pointCount = kArrayList_Count(pointList);
    if (pointCount >= 4)
    {
        for (ix = 0; ix < pointCount; ++ix)
        {
            pointPtr = (kPoint3d64f*)kArrayList_At(pointList, ix);

            // distance
            k64f distance = MATH_DIST(pointPtr, center);
            k64f diff = distance - radius;

            Xsum += diff * diff;
        }

        var = Xsum / (pointCount - 1);
    }

    *stdev = sqrt(var);
    }
        kCatchEx(&exception)
    {
        kEndCatchEx(exception);
    }
    kFinallyEx
    {
        kEndFinallyEx();
    }

    return kOK;
}

GtsFx(kStatus) GdkProfileEllipseFit_ellipse2Poly(cv::Point2d center, cv::Size2d axes, int angle,
    int arc_start, int arc_end,
    int delta, kArrayList* pts, kAlloc alloc)
{

    k64f alpha, beta;
    int i;

    while (angle < 0)
        angle += 360;
    while (angle > 360)
        angle -= 360;

    if (arc_start > arc_end)
    {
        i = arc_start;
        arc_start = arc_end;
        arc_end = i;
    }
    while (arc_start < 0)
    {
        arc_start += 360;
        arc_end += 360;
    }
    while (arc_end > 360)
    {
        arc_end -= 360;
        arc_start -= 360;
    }
    if (arc_end - arc_start > 360)
    {
        arc_start = 0;
        arc_end = 360;
    }
    alpha = MATH_COS_DEG(angle); beta = MATH_SIN_DEG(angle);
    //my_sincos(angle, alpha, beta);

    kCheck(kArrayList_Construct(pts, kTypeOf(kPoint3d32f), 0, alloc));

    for (i = arc_start; i < arc_end + delta; i += delta)
    {
        double x, y;
        angle = i;
        if (angle > arc_end)
            angle = arc_end;
        if (angle < 0)
            angle += 360;

        x = axes.width * MATH_SIN_DEG(450 - angle);
        y = axes.height * MATH_SIN_DEG(angle);
        kPoint3d32f pt = {0,0,0};
        pt.x = (k32f)(center.x + x * alpha - y * beta);
        pt.z = (k32f)(center.y + x * beta + y * alpha);
        kCheck(kArrayList_Add(*pts, &pt));
    }

    // If there are no points, it's a zero-size polygon
    if (kArrayList_Count(*pts) == 1) {
        kCheck(kArrayList_Add(*pts, &center));
    }

    return kOK;
}

/////////////////////////////////////////////////////////////////////////////
// Output rendering
/////////////////////////////////////////////////////////////////////////////

// Output a measurement value (no graphics)
GtsFx(kStatus) GdkProfileEllipseFit_OutputValue(GdkToolCfg config, kSize index, k64f value, kBool valid, GdkToolOutput output)
{
    GvMeasureMsg msg = kNULL;

    if (index < GdkToolCfg_MeasurementCount(config) &&
        GdkMeasurementCfg_Enabled(GdkToolCfg_MeasurementAt(config, index)))
    {
        kCheck(GdkToolOutput_InitMeasurementAt(output, index, &msg));
        if (msg != kNULL)
        {
            if (valid)
            {
                kCheck(GvMeasureMsg_SetValue(msg, value));
                kCheck(GvMeasureMsg_SetStatus(msg, GV_MEASUREMENT_OK));
            }
            else
            {
                kCheck(GvMeasureMsg_SetStatus(msg, GV_MEASUREMENT_ERROR_INVALID_RESULT));
            }
        }
    }

    return kOK;
}

// Output a measurement value and draw a circle
GtsFx(kStatus) GdkProfileEllipseFit_OutputValueAndEllipse(GdkToolCfg config, kSize index, cv::RotatedRect ellipseRect, EllipseFeature feature, GdkToolOutput output, kAlloc alloc)
{
    GdkGraphic graphic = kNULL;
    GdkGraphicLineSet lineSet = kNULL;
    GdkGraphicPointSet pointSet = kNULL;
    kStatus exception = kOK;
    GvMeasureMsg msg = kNULL;
    std::vector<cv::Point2d> ellipsePoints;
    kArray1 pointList = kNULL;
    k64f value = k64F_NULL;
    k32f majorAngle = 0;

    kTry
    {
        if (index < GdkToolCfg_MeasurementCount(config) &&
            GdkMeasurementCfg_Enabled(GdkToolCfg_MeasurementAt(config, index)))
        {
            kTest(GdkGraphic_Construct(&graphic, alloc));

            // Create the ellipse points
            GdkProfileEllipseFit_ellipse2Poly((cv::Point2d)ellipseRect.center, cv::Size2d(ellipseRect.size.width/2, ellipseRect.size.height/ 2),
                                                (int)ellipseRect.angle, 0, 360, 5, &pointList, alloc);
            //kTest(kArray1_Construct(&pointList, kTypeOf(kPoint3d32f), ellipsePoints.size(), alloc));
            //for (int i = 0; i < ellipsePoints.size(); i++)
            //{
            //    kPoint3d32f pt = { ellipsePoints[i].x, 0, ellipsePoints[i].y };
            //    kTest(kArray1_SetItem(pointList, i, &pt));
            //}

            if(feature == ELLIPSE_CENTERX || feature == ELLIPSE_CENTERY || feature == ELLIPSE_ANGLE)
            {
                kPoint3d32f center32f = { ellipseRect.center.x, 0, ellipseRect.center.y };
                kTest(GdkGraphicPointSet_Construct(&pointSet, 2.0, kMARKER_SHAPE_PLUS, kCOLOR_WHITE, &center32f, 1, alloc));
                kTest(GdkGraphic_AddPointSet(graphic, pointSet));
                pointSet = kNULL;

                value = feature == ELLIPSE_ANGLE? ellipseRect.angle : feature == ELLIPSE_CENTERX? ellipseRect.center.x : ellipseRect.center.y;
            }
            else
            {
                //if (ellipseRect.size.width < ellipseRect.size.height) {
                //    float tmp = ellipseRect.size.width;
                //    ellipseRect.size.width = ellipseRect.size.height;
                //    ellipseRect.size.height = tmp;
                //    ellipseRect.angle += 90;
                //}

                kPoint3d32f axisPoints[2] = {{0,0,0},{0,0,0}};
                if (ellipseRect.size.width > ellipseRect.size.height)
                {
                    majorAngle = ellipseRect.angle;
                }
                else
                {
                    majorAngle = ellipseRect.angle + 90;
                }
                if (feature == ELLIPSE_MAJOR)
                {
                    value = kMax_(ellipseRect.size.width, ellipseRect.size.height) / 2;

                    kPoint3d_Init_(&axisPoints[0],
                        (k32f)(ellipseRect.center.x - MATH_COS_DEG(majorAngle) * value), 0, (k32f)(ellipseRect.center.y - MATH_SIN_DEG(majorAngle) * value)
                    );
                    kPoint3d_Init_(&axisPoints[1],
                        (k32f)(ellipseRect.center.x + MATH_COS_DEG(majorAngle) * value), 0, (k32f)(ellipseRect.center.y + MATH_SIN_DEG(majorAngle) * value)
                    );
                }
                else
                {
                    value = kMin_(ellipseRect.size.width, ellipseRect.size.height) / 2;

                    kPoint3d_Init_(&axisPoints[0],
                        (k32f)(ellipseRect.center.x - MATH_COS_DEG(majorAngle + 90) * value), 0, (k32f)(ellipseRect.center.y - MATH_SIN_DEG(majorAngle + 90) * value)
                    );
                    kPoint3d_Init_(&axisPoints[1],
                        (k32f)(ellipseRect.center.x + MATH_COS_DEG(majorAngle + 90) * value), 0, (k32f)(ellipseRect.center.y + MATH_SIN_DEG(majorAngle + 90) * value)
                    );
                }

                kTest(GdkGraphicLineSet_Construct(&lineSet, 2.0, kCOLOR_WHITE, axisPoints, 2, alloc));
                kTest(GdkGraphic_AddLineSet(graphic, lineSet));
                lineSet = kNULL;
            }

            // Draw Ellipse
            if (pointList != kNULL)
            {
                kTest(GdkGraphicLineSet_Construct(&lineSet, 2.0, kCOLOR_LIGHT_BLUE, (const kPoint3d32f*)kArrayList_Data(pointList), kArrayList_Count(pointList), alloc));
                kTest(GdkGraphic_AddLineSet(graphic, lineSet));
                lineSet = kNULL;
            }

            kTest(GdkToolOutput_SetRendering(output, index, graphic));
            graphic = kNULL;

            kTest(GdkToolOutput_InitMeasurementAt(output, index, &msg));
            if (msg != kNULL)
            {
                if (value != k64F_NULL)
                {
                    kTest(GvMeasureMsg_SetValue(msg, value));
                    kTest(GvMeasureMsg_SetStatus(msg, GV_MEASUREMENT_OK));
                }
                else
                {
                    kTest(GvMeasureMsg_SetStatus(msg, GV_MEASUREMENT_ERROR_INVALID_RESULT));
                }
            }
        }
    }
    kCatch(&exception)
    {
        kDestroyRef(&graphic);
        kDestroyRef(&lineSet);
        kDestroyRef(&pointSet);
        kEndCatch(exception);
    }

    return kOK;
}

// Output the center as a point feature
GtsFx(kStatus) GdkProfileEllipseFit_SendFeaturePoint(GdkProfileEllipseFit tool, GdkToolOutput output, kSize index, const kPoint64f* point)
{
    GdkCircleFeature feature = kNULL;
    kPoint3d64f point3d = { point->x, 0, point->y };

    feature = (GdkCircleFeature)GdkToolOutput_FeatureAt(output, index);

    kCheck(GdkFeature_SetType(feature, GDK_FEATURE_TYPE_POINT));
    kCheck(GdkPointFeature_SetPosition(feature, &point3d));

    return kOK;
}
