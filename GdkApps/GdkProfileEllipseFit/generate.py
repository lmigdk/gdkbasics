#!/usr/bin/python

import os
import sys

# The xml file for the solution and project
solutionPaths = [                      '.']
solutionNames = ['GdkProfileEllipseFitSln']
projectNames  = [   'GdkProfileEllipseFit']

def GenerateUserFile(pPath, pName):
    fileString = """<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="12.0" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|x64'">
    <LocalDebuggerCommand>$(TargetDir)/GoEmulator.exe</LocalDebuggerCommand>
    <DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|x64'">
    <LocalDebuggerCommand>$(TargetDir)/GoEmulator.exe</LocalDebuggerCommand>
    <DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Debug|Win32'">
    <LocalDebuggerCommand>$(TargetDir)/GoEmulator.exe</LocalDebuggerCommand>
    <DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)|$(Platform)'=='Release|Win32'">
    <LocalDebuggerCommand>$(TargetDir)/GoEmulator.exe</LocalDebuggerCommand>
    <DebuggerFlavor>WindowsLocalDebugger</DebuggerFlavor>
  </PropertyGroup>
</Project>"""
    
    fileName = os.path.join(pPath, pName+'.vcxproj.user')
    if not os.path.exists(fileName):
        file = open(fileName, "w")
        file.write(fileString)
        file.close()
        
def GenerateChildDebugFile(sPath, sName):
    fileString = """<?xml version="1.0" encoding="utf-8"?>
<ChildProcessDebuggingSettings IsEnabled="true" xmlns="http://schemas.microsoft.com/vstudio/ChildProcessDebuggingSettings/2014">
  <DefaultRule EngineFilter="[inherit]" />
</ChildProcessDebuggingSettings>"""
    
    fileName = os.path.join(sPath, sName+'.ChildProcessDbgSettings')
    if not os.path.exists(fileName):
        file = open(fileName, "w")
        file.write(fileString)
        file.close()
        
def GenerateSolution(sPath, sName, pPath, pName):
    print('Generating Gdk solution')
    import Platform.scripts.Utils.kGenerator as generator
    
    # Generate Visual Studios project to be able to compile for the WIN32/64,
    # Linux X64 platforms and the special Sensor platform.
    # For the Linux X64, this does NOT create make files. It only adds Linux X64
    # as a platform that is selectable from with VS, for development convenience.
    generator.WriteSolution(os.path.join(sPath, sName+'.xml'), ['MsvcChooser'], ['Win32', 'Win64', 'Sensor', 'Linux_X64'])
    # Generate make files for embedded platforms.
    generator.WriteSolution(os.path.join(sPath, sName+'.xml'), ['WrWbMk'], ['Arm7'])
    generator.WriteSolution(os.path.join(sPath, sName+'.xml'), ['GnuMk_Linux_Arm64', 'GnuMk_Linux_X64'], ['Linux_Arm64', 'Linux_X64'])

    GenerateUserFile(pPath, pName)
    GenerateChildDebugFile(sPath, sName)
    
def Generate():
    herePath = os.path.dirname(os.path.realpath(__file__))
    workDir = os.path.normpath(os.path.join(herePath, '..', '..', '..'))
    sys.path.append(workDir)
    
    for sp, sn, pn in zip(solutionPaths, solutionNames, projectNames):
        GenerateSolution(os.path.normpath(os.path.join(herePath, sp)), sn, herePath, pn)
    
if __name__ == '__main__':
    Generate()
