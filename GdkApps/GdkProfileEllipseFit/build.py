#!/usr/bin/python

import sys
import os
import subprocess
import argparse

def GetProjectName():
    herePath = os.path.dirname(os.path.realpath(__file__))
    return herePath.split(os.path.sep)[-1]

def Build(config, projectName, cleanTargets, buildTargets, updatePackage):
    herePath = os.path.dirname(os.path.realpath(__file__))
    pkgUpdatePath = os.path.join(herePath, '..', '..', '..', 'bin', 'win32', 'GoPkgUpdate.exe')
    gmakePath = os.path.join(herePath, '..', '..', '..', 'Platform', 'extern', 'Make', 'gmake.exe')
    if not projectName:
        projectName = GetProjectName()

    makeGoals = []

    if cleanTargets:
        makeGoals.append('clean')

    if buildTargets:
        makeGoals.append('all')

    print('Building ' + projectName + ' for Arm', flush=True)
    subprocess.check_call([gmakePath, '-f', projectName + '-WrWb.mk', 'config=' + config] + makeGoals)

    print('Building ' + projectName + ' for C64x', flush=True)
    subprocess.check_call([gmakePath, '-f', projectName + '-Ccs6.mk', 'config=' + config] + makeGoals)
    subprocess.check_call([gmakePath, '-f', projectName + '-Linux_Arm64.mk', 'config=' + config] + makeGoals)

    if updatePackage:
        print('Creating custom package for ' + projectName, flush=True)
        subprocess.check_call([pkgUpdatePath, projectName])

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Build for ARM and C64x targets.')

    parser.add_argument('config', help='Configuration name.')
    parser.add_argument('--project', help='Project name.', default='')
    parser.add_argument('--package', action='store_true', help='Project name.')
    
    group = parser.add_mutually_exclusive_group()
    group.add_argument('--rebuild', action='store_true', help='Rebuild target files.')
    group.add_argument('--clean', action='store_true', help='Clean target files.')

    args = parser.parse_args()

    cleanTargets = (args.rebuild or args.clean)             # Clean on rebuild and clean
    buildTargets = (not args.clean)                         # Always build except when cleaning
    updatePackage = (buildTargets and args.package)         # Update the package if building and specified

    Build(args.config, args.project, cleanTargets, buildTargets, updatePackage)
